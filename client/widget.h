#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QSslConfiguration>
#include <QNetworkReply>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void onFinished(QNetworkReply* reply);

private:
    Ui::Widget *ui;
    QNetworkAccessManager* client;
    QSslConfiguration sslconfig;
};

#endif // WIDGET_H
